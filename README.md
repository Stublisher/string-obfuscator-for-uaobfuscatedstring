##Abstract##
A simple python script to generate a string that can be copied/pasted into XCode to obfuscate a string using [UrbanApp](https://github.com/UrbanApps "UrbanApp")'s [UAObfuscatedString](https://github.com/UrbanApps/UAObfuscatedString "UAObfuscatedString").
## Usage ##

```
#!bash

$ python obfuscate.py -s "Obfuscate this string."
Obfuscate.O.b.f.u.s.c.a.t.e.space.t.h.i.s.space.s.t.r.i.n.g.dot;  // copy and paste in to XCode!

$ python obfuscate.py -f obfuscate_this_file.txt
# will read, obfuscate and replace the contents of the text file.
```