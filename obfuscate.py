import sys

def main():
	if len(sys.argv) != 3: print "usage: python obfuscate.py -s \"string_to_obfuscate\"\nusage: python obfuscate.py -f file_to_obsfucate.txt"
	else:
		if str(sys.argv[1]) == "-s":
			print obfuscate(str(sys.argv[2]))
		elif str(sys.argv[1]) == "-f":
			print obf_file(str(sys.argv[2]))
		else:
			print "invalid flag: " + str(sys.argv[1])

def obf_file(file):
	f = open(file, 'r+')
	text = f.read()
	print ">> : " + text
	obf_t = obfuscate(text)
	f.seek(0)
	f.truncate
	f.write(obf_t)
	f.close()
	return "Successfully modified: " + file

def obfuscate(message):
    ob = "Obfuscate."
    for c in message:
        s = stringForChar(c)
        if (s == ""): continue
        ob = ob + s + "."
    return ob[:-1] + ";"

def stringForChar(char):
    a = ord(char)
    if a >= 48 and a <= 57: # number, add underscore
        return "_" + char
    elif a >= 65 and a <= 90 or a >= 97 and a <= 122: # letter
        return char
    elif a == 95 or a == 36: return char # _ or $ (respectively)
    elif a == 32: return "space"
    elif a == 46: return "dot"
    elif a == 45: return "dash"
    elif a == 44: return "comma"
    elif a == 59: return "semicolon"
    elif a == 58: return "colon"
    elif a == 39: return "apostrophe"
    elif a == 34: return "quotation"
    elif a == 43: return "plus"
    elif a == 61: return "equals"
    elif a == 40: return "paren_left"
    elif a == 41: return "paren_right"
    elif a == 42: return "asterisk"
    elif a == 38: return "ampersand"
    elif a == 94: return "caret"
    elif a == 37: return "percent"
    elif a == 35: return "pound"
    elif a == 64: return "at"
    elif a == 33: return "exclamation"
    elif a == 92: return "back_slash"
    elif a == 47: return "forward_slash"
    elif a == 123: return "curly_left"
    elif a == 125: return "curly_right"
    elif a == 91: return "bracket_left"
    elif a == 93: return "bracket_right"
    elif a == 124: return "bar"
    elif a == 60: return "less_than"
    elif a == 62: return "greater_than"
    else: return ""

if __name__ == '__main__':
  main()
